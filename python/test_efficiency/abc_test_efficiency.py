import numpy as np

from abc import ABC, abstractmethod
from time import time
from typing import Callable


class TestEfficiency(ABC):
    def __init__(
        self,
        sensor1,
        sensor2,
        n_points,
        n_kernel_eigenvectors,
        n_jointly_smooth_functions,
        kernel,
        kernel_eigenvalue_cut_off,
        eigenvector_tolerance,
        get_peff,
        random_seed,
        verbose,
        dist_kwargs,
    ):
        self.sensor1 = sensor1
        self.sensor2 = sensor2
        self.n_points = n_points
        self.n_kernel_eigenvectors = n_kernel_eigenvectors
        self.n_jointly_smooth_functions = n_jointly_smooth_functions
        self.kernel = kernel
        self.kernel_eigenvalue_cut_off = kernel_eigenvalue_cut_off
        self.eigenvector_tolerance = eigenvector_tolerance
        self.get_peff = get_peff
        self.random_seed = random_seed
        self.verbose = verbose
        self.dist_kwargs = dist_kwargs

        self.time_data = {
            'n_points': self.n_points,
            'n_kernel_eigenvectors': self.n_kernel_eigenvectors,
            'n_jointly_smooth_functions': self.n_jointly_smooth_functions,
            'random_seed': self.random_seed,
        }

    @abstractmethod
    def compute_common_system(self):
        pass

    def setup_example(self):
        s1, s2, peff = self._time_method('time_setup_examples', self._setup_examples)
        return s1, s2, peff

    def _setup_examples(self):
        rng = np.random.default_rng(self.random_seed)

        xyz = rng.uniform(low=-0.5, high=0.5, size=(self.n_points, 3))
        x = xyz[:, 0].reshape(-1, 1)
        y = xyz[:, 1].reshape(-1, 1)
        z = xyz[:, 2].reshape(-1, 1)

        s1 = self.sensor1(x, y)

        if self.get_peff is None:
            peff = s1[:, 0] + 1 * s1[:, 1] ** 2
        else:
            peff = self.get_peff(x, y, z)

        s2 = self.sensor2(peff, z[:, 0])

        return s1, s2, peff

    def _time_method(self, time_data_key: str, method: Callable):
        t0 = time()
        result = method()
        elapsed_time = time() - t0
        self.time_data[time_data_key] = elapsed_time
        return result

    def _log(self, message):
        if self.verbose:
            print(message)
