import datafold.pcfold as pfold
import numpy as np

from jointly_smooth_functions import JointlySmoothFunctions
from time import time


class TestEfficiency():
    def __init__(self, sensor1, sensor2, verbose=True, n_pts=10000, random_seed=1, n_kernel_evecs=500,
                 n_common_evecs=10, cutoff_result_scaling=1, kernel_tol=1e-8, evec_tol=1e-6, cknn_k_neighbor=50,
                 cknn_delta=1, compute_sparse=True, get_peff=None, kernel_type="gaussian"):
        # parameters
        self.n_pts = n_pts
        self.random_seed = random_seed

        self.verbose = verbose
        self.n_kernel_normalizations = 3
        self.evec_tol = evec_tol
        self.kernel_tol = kernel_tol
        self.cknn_k_neighbor = cknn_k_neighbor
        self.cknn_delta = cknn_delta

        self.n_kernel_evecs = n_kernel_evecs
        self.n_common_evecs = n_common_evecs

        self.compute_sparse = compute_sparse

        self.sensor1 = sensor1
        self.sensor2 = sensor2
        self.get_peff = get_peff

        self.cutoff_result_scaling = cutoff_result_scaling

        self.kernel_type = kernel_type

        self.time_data = dict()
        self.time_data["n_pts"] = self.n_pts
        self.time_data["n_kernel_evecs"] = self.n_kernel_evecs
        self.time_data["n_common_evecs"] = self.n_common_evecs
        self.time_data["random_seed"] = self.random_seed

    def __log(self, message):
        if self.verbose:
            print(message)

    def setup_example(self):
        t0 = time()

        rng = np.random.default_rng(self.random_seed)

        xyz = rng.uniform(low=-.5, high=.5, size=(self.n_pts, 3))
        x, y, z = xyz[:, 0].reshape(-1, 1), xyz[:, 1].reshape(-1, 1), xyz[:, 2].reshape(-1, 1)

        s1 = self.sensor1(x, y)

        if self.get_peff is None:
            peff = s1[:, 0] + 1 * s1[:, 1] ** 2
        else:
            peff = self.get_peff(x, y, z)

        s2 = self.sensor2(peff, z[:, 0])

        self.time_data["time_example"] = time() - t0
        return s1, s2, peff

    def compute_common_system(self):
        s1, s2, peff = self.setup_example()

        kernel = pfold.kernels.ContinuousNNKernel(k_neighbor=self.cknn_k_neighbor, delta=self.cknn_delta)
        jsf = JointlySmoothFunctions(
            n_kernel_eigenvectors=self.n_kernel_evecs,
            n_jointly_smooth_functions=self.n_common_evecs,
            kernel=kernel,
            kernel_eigenvalue_cut_off=self.kernel_tol,
            eigenvector_tolerance=self.evec_tol,
            cut_off = 1e-16,
            backend='scipy.kdtree',
        )
        jsf.fit(np.column_stack([s1, s2]), ending_points=[s1.shape[1], s1.shape[1]+s2.shape[1]])

        return jsf.jointly_smooth_functions, jsf.eigenvalues