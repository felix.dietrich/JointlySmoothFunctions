import numpy as np

from jointly_smooth_functions import JointlySmoothFunctions
from test_efficiency.abc_test_efficiency import TestEfficiency


class JSFTestEfficiency(TestEfficiency):
    def __init__(
        self,
        sensor1,
        sensor2,
        n_points,
        n_kernel_eigenvectors,
        n_jointly_smooth_functions,
        kernel,
        kernel_eigenvalue_cut_off,
        eigenvector_tolerance,
        get_peff,
        random_seed,
        verbose,
        dist_kwargs = None,
    ):
        super().__init__(
            sensor1=sensor1,
            sensor2=sensor2,
            n_points=n_points,
            n_kernel_eigenvectors=n_kernel_eigenvectors,
            n_jointly_smooth_functions=n_jointly_smooth_functions,
            kernel=kernel,
            kernel_eigenvalue_cut_off=kernel_eigenvalue_cut_off,
            eigenvector_tolerance=eigenvector_tolerance,
            get_peff=get_peff,
            random_seed=random_seed,
            verbose=verbose,
            dist_kwargs=dist_kwargs,
        )

    def compute_common_system(self):
        s1, s2, peff = self.setup_example()

        jsf = JointlySmoothFunctions(
            n_kernel_eigenvectors=self.n_kernel_eigenvectors,
            n_jointly_smooth_functions=self.n_jointly_smooth_functions,
            kernel=self.kernel,
            kernel_eigenvalue_cut_off=self.kernel_eigenvalue_cut_off,
            eigenvector_tolerance=self.eigenvector_tolerance,
            **self.dist_kwargs,
        )

        self._time_method(
            'time_jsf_calculation',
            lambda: jsf.fit(np.column_stack([s1, s2]), ending_points=[s1.shape[1], s1.shape[1] + s2.shape[1]])
        )

        time_elapsed = self.time_data['time_jsf_calculation']
        self._log(f'Time for SVD: {time_elapsed}')

        return jsf.eigenvalues, jsf.jointly_smooth_functions
