# Spectral Discovery of Jointly Smooth Features for Multimodal Data

The original implementation of the paper:
Spectral Discovery of Jointly Smooth Features for Multimodal Data

The NCCA results in MATLAB can be found here:
[RonenTalmonLab at github.com](https://github.com/RonenTalmonLab/JointlySmoothFunctions)

An efficient implementation in the ``datafold'' package can be found here:
[datafold software tutorial](https://datafold-dev.gitlab.io/datafold/tutorial_07_basic_jsf_common_eigensystem.html)